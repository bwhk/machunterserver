package tho.dev.tools;


import org.junit.After;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import tho.dev.tools.config.TenantContext;
import tho.dev.tools.mapper.DeviceMapper;

import java.util.List;
import java.util.UUID;

@RunWith(SpringRunner.class)
@SpringBootTest
@TestPropertySource("/application.yml")
public class MacHunterServerApplicationTests {

	private static final long DEFAULT_ACCOUNT_ID = 1L;
	private static final long DEFAULT_USER_ID = 2L;

	@Autowired
	DeviceMapper deviceMapper;

	private static Device testDevice1;
	private static Device testDevice2;
	private static Device testDevice3;

	@BeforeClass
	public static void initDevices() {
		TenantContext.setCurrentTenant(TenantContext.defaultTenantKey);
		testDevice1 = createDummyDevice();
		testDevice2 = createDummyDevice();
		testDevice3 = createDummyDevice();
	}

	@After
	public void cleanTests() {
		deviceMapper.delete(DEFAULT_ACCOUNT_ID, DEFAULT_USER_ID, testDevice1.getId());
		deviceMapper.delete(DEFAULT_ACCOUNT_ID, DEFAULT_USER_ID, testDevice2.getId());
		deviceMapper.delete(DEFAULT_ACCOUNT_ID, DEFAULT_USER_ID, testDevice3.getId());
	}

	@Test
	public void testGetAllDevices() {
		deviceMapper.add(DEFAULT_ACCOUNT_ID, DEFAULT_USER_ID, testDevice1);
		deviceMapper.add(DEFAULT_ACCOUNT_ID, DEFAULT_USER_ID, testDevice2);
		deviceMapper.add(DEFAULT_ACCOUNT_ID, DEFAULT_USER_ID, testDevice3);

		final List<Device> devices = deviceMapper.getAll(DEFAULT_ACCOUNT_ID, DEFAULT_USER_ID);
		Assert.assertEquals("Size is wrong!", 3, devices.size());
	}

	@Test
	public void testCreateDeviceWithOtherUserIDAndTryToRequestWithDefaultId() {
		deviceMapper.add(DEFAULT_ACCOUNT_ID + 1L, DEFAULT_USER_ID + 1L, testDevice1);
		try {
			final Device device = deviceMapper.get(DEFAULT_ACCOUNT_ID, DEFAULT_USER_ID, testDevice1.getId());
			if (device != null) {
				Assert.fail("No exception thrown and device is not null!");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static Device createDummyDevice() {
		final Device device = new Device();
		device.setActive(true);
		device.setDescription(getRandomString());
		device.setIp(getRandomString());
		device.setMacIdentifier(getRandomString());
		device.setName(getRandomString());
		device.setSnmpVersion(SnmpVersion.V2C);
		return device;
	}

	private static String getRandomString() {
		return UUID.randomUUID().toString();
	}
}
