package tho.dev.tools.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import tho.dev.tools.Device;
import tho.dev.tools.config.TenantContext;
import tho.dev.tools.mapper.DeviceMapper;
import java.util.List;

/**
 * Device Service class which handle device business logic
 * Created by tonyhorst on 04.12.16.
 */
@Component
public class DeviceService {

    /** MyBatis Mapper Interface. */
    @Autowired
    private DeviceMapper deviceMapper;

    /**
     * Get one device.
     * @param accountId accountId
     * @param userId userId
     * @param deviceId id from requested device
     * @return null or device
     */
    public Device get(final long accountId, final long userId, final long deviceId) {
        TenantContext.setCurrentTenant(TenantContext.defaultTenantKey);
        return deviceMapper.get(accountId, userId, deviceId);
    }

    /**
     * Get all devices.
     * @param accountId given accountId.
     * @param userId given userId.
     * @return List with all devices from account/user
     */
    public List<Device> getAll(final long accountId, final long userId) {
        TenantContext.setCurrentTenant(TenantContext.defaultTenantKey);
        return deviceMapper.getAll(accountId, userId);
    }

    /**
     * Add a new device
     * @param accountId given accountId
     * @param userId given userId
     * @param device new device
     * @return device with updated database id
     */
    public Device add(final long accountId, final long userId, final Device device) {
        TenantContext.setCurrentTenant(TenantContext.defaultTenantKey);
        deviceMapper.add(accountId, userId, device);
        return device;
    }

    /**
     * Edit a specific device
     * @param accountId given accountId
     * @param userId given userId
     * @param deviceId given deviceId
     * @param device new deviceData
     * @return true if edited, else false
     */
    public boolean edit(final long accountId, final long userId, final long deviceId, final Device device) {
        TenantContext.setCurrentTenant(TenantContext.defaultTenantKey);
        final int edit = deviceMapper.edit(accountId, userId, deviceId, device);
        return edit > 0;
    }

    /**
     * Remove a specific device
     * @param accountId given accountId
     * @param userId given userId
     * @param device given device
     * @return true if removed, else false
     */
    public boolean remove(final long accountId, final long userId, final long device) {
        TenantContext.setCurrentTenant(TenantContext.defaultTenantKey);
        final int delete = deviceMapper.delete(accountId, userId, device);
        return delete > 0;
    }
}
