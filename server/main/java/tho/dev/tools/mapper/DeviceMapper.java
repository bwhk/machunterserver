package tho.dev.tools.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import tho.dev.tools.Device;

import java.util.List;

/**
 * MyBatis Device Table Mapper.
 */
@Mapper
public interface DeviceMapper {

    /**
     * Request a device from database
     * @param accountId given accountId
     * @param userId given userId
     * @param deviceId given deviceId
     * @return device or null
     */
    Device get(@Param("accountId") long accountId, @Param("userId") long userId, @Param("deviceId") final long deviceId);

    /**
     * Request a list from all devices in database
     * @param accountId given accountId
     * @param userId given userId
     * @return a list of all devices
     */
    List<Device> getAll(@Param("accountId") final long accountId, @Param("userId") final long userId);

    /**
     * Add a device
     * @param accountId given accountId
     * @param userId given userId
     * @param device new device
     */
    void add(@Param("accountId") final long accountId, @Param("userId") final long userId, @Param("device") final Device device);

    /**
     * Edit a device
     * @param accountId given accountId
     * @param userId given userId
     * @param deviceId given deviceId
     * @param device new device data
     * @return number of edited rows
     */
    int edit(@Param("accountId") final long accountId, @Param("userId") final long userId, @Param("deviceId") final long deviceId, @Param("device") final Device device);

    /**
     * Remove a device
     * @param accountId given accountId
     * @param userId given userId
     * @param deviceId given deviceId
     * @return number of deleted rows
     */
    int delete(@Param("accountId") long accountId, @Param("userId") long userId, @Param("deviceId") final long deviceId);
}
