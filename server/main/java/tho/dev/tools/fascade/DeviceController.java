package tho.dev.tools.fascade;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tho.dev.tools.Device;
import tho.dev.tools.service.DeviceService;

import java.util.List;

/**
 * REST Controller class for devices with default request mapping path
 */
@RestController
@RequestMapping("/accounts/{accountId}/users/{userId}/users/devices")
public class DeviceController {

    @Autowired
    private DeviceService deviceService;

    /**
     * Request all devices
     * @param accountId given accountId from global path
     * @param userId given userId from global path
     * @return Response Entity with all devices
     */
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ResponseEntity<List<Device>> getAll(@PathVariable("accountId") final long accountId,
                                               @PathVariable("userId") final long userId)
    {
        return new ResponseEntity<>(deviceService.getAll(accountId, userId), HttpStatus.OK);
    }

    /**
     * Request one device
     * @param accountId given accountId from global path
     * @param userId given userId from global path
     * @param deviceId requested device id
     * @return Response Entity with one device
     */
    @RequestMapping(value = "/{deviceId}", method = RequestMethod.GET)
    public ResponseEntity<Device> get(@PathVariable("accountId") final long accountId,
                      @PathVariable("userId") final long userId,
                      @PathVariable("deviceId") final long deviceId)
    {
        final Device device = deviceService.get(accountId, userId, deviceId);
        if (device == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<>(device, HttpStatus.OK);
        }
    }

    /**
     * Add one device
     * @param accountId given accountId from global path
     * @param userId given userId from global path
     * @param device device data
     * @return Response Entity with added device
     */
    @RequestMapping(value = "/", method = RequestMethod.POST)
    public Device add(@PathVariable("accountId") final long accountId,
                      @PathVariable("userId") final long userId,
                      @RequestBody final Device device)
    {
        return deviceService.add(accountId, userId, device);
    }

    /**
     * Edit one device
     * @param accountId given accountId from global path
     * @param userId given userId from global path
     * @param deviceId given deviceId
     * @param device device data
     * @return OK with edited device, NOT FOUND if not found
     */
    @RequestMapping(value = "/{deviceId}", method = RequestMethod.PUT)
    public ResponseEntity<Device> edit(@PathVariable("accountId") final long accountId,
                                       @PathVariable("userId") final long userId,
                                       @PathVariable("deviceId") final long deviceId,
                                       @RequestBody final Device device)
    {
        if (deviceService.edit(accountId, userId, deviceId, device)) {
            device.setId(deviceId);
            return new ResponseEntity<>(device, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Remove one device
     * @param accountId given accountId from global path
     * @param userId given userId from global path
     * @param deviceId given deviceId
     * @return OK when removed, NOT FOUND if not found
     */
    @RequestMapping(value = "/{deviceId}", method = RequestMethod.DELETE)
    public ResponseEntity delete(@PathVariable("accountId") final long accountId,
                         @PathVariable("userId") final long userId,
                         @PathVariable("deviceId") final long deviceId) {
        if (deviceService.remove(accountId, userId, deviceId)) {
            return new ResponseEntity<>(HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
