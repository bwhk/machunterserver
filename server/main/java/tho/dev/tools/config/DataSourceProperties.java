package tho.dev.tools.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Configuration class represent multi-tenant data source configuration
 */
@Component("dataSourceProperties")
@ConfigurationProperties("db")
public class DataSourceProperties {


    private Map<String, DataSourceConfig> datasource = new LinkedHashMap<>();

    public Map<String, DataSourceConfig> getDatasource() {
        return datasource;
    }

    public static class DataSourceConfig {


        /**
         * Fully qualified name of the JDBC driver. Auto-detected based on the URL by default.
         */
        private String driverClassName;

        /**
         * JDBC url of the database.
         */
        private String url;

        /**
         * Login user of the database.
         */
        private String username;

        /**
         * Login password of the database.
         */
        private String password;


        /**
         * JNDI name used to reference account data source
         */
        private String legacyReference;


        /**
         * Flag that indicates whether connection is default or primary.
         */
        private Boolean defaultConnection = false;


        public String getDriverClassName() {
            return driverClassName;
        }

        public void setDriverClassName(String driverClassName) {
            this.driverClassName = driverClassName;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getLegacyReference() {
            return legacyReference;
        }

        public void setLegacyReference(String legacyReference) {
            this.legacyReference = legacyReference;
        }

        public Boolean isDefaultConnection() {
            return defaultConnection;
        }

        public void setDefaultConnection(Boolean defaultConnection) {
            this.defaultConnection = defaultConnection;
        }
    }

}
