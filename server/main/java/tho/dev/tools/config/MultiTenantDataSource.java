package tho.dev.tools.config;

import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;

/**
 * JNDI Lookup configurations (for future ==> account based databases)
 */
public class MultiTenantDataSource extends AbstractRoutingDataSource {

    @Override
    protected Object determineCurrentLookupKey() {
        return TenantContext.getCurrentTenant();
    }

}