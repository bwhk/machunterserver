package tho.dev.tools.config;

/**
 * Context Holder class
 */
public class TenantContext {

    /**
     * Key represent default tenant, used for global device/node/user database
     */
    public static String defaultTenantKey = "jdbc/default";

    private static final ThreadLocal<String> tenant = new ThreadLocal<String>();

    public static String getCurrentTenant() {
        return tenant.get();
    }

    public static void setCurrentTenant(String dbKey) {
        tenant.set(dbKey);
    }


}