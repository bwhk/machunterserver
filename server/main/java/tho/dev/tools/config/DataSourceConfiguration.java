package tho.dev.tools.config;


import org.apache.ibatis.datasource.pooled.PooledDataSource;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.type.JdbcType;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.mapper.MapperScannerConfigurer;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

@Configuration
public class DataSourceConfiguration  implements ApplicationContextAware {

    private AutowireCapableBeanFactory beanFactory;

    private MultiTenantDataSource routingDataSource;

    public static final String SQL_SESSION_FACTORY_NAME_1 = "sqlSessionFactory";

    public static final String MAPPERS_PACKAGE_NAME_1 = "tho.dev.tools.mapper";


    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        beanFactory = applicationContext.getAutowireCapableBeanFactory();
    }

    @Bean
    public MultiTenantDataSource getDataSource(){
        if(routingDataSource == null){
            DataSourceProperties config2 = (DataSourceProperties)beanFactory.getBean("dataSourceProperties");

            routingDataSource = new MultiTenantDataSource();
            Map<Object, Object> targetDataSources = new HashMap<>();

            //Loop over existing data source config and create DataSource objects
            Map<String, DataSourceProperties.DataSourceConfig> datasource = config2.getDatasource();
            for (String key: datasource.keySet()) {
                DataSourceProperties.DataSourceConfig dataSourceConfig = datasource.get(key);
                DataSource dataSource = pooledDataSource(dataSourceConfig);
                if(dataSourceConfig.isDefaultConnection()){
                    //this is the default connection to be used
                    routingDataSource.setDefaultTargetDataSource(dataSource);
                    // this is the identifier of the default tenant
                    // used for switching tenant to get account configuration
                    TenantContext.defaultTenantKey = dataSourceConfig.getLegacyReference();
                }
                //Use legacy name to identify which account to switch
                targetDataSources.put(dataSourceConfig.getLegacyReference(), dataSource);
            }
            routingDataSource.setTargetDataSources(targetDataSources);
            routingDataSource.afterPropertiesSet();
        }

        return routingDataSource;
    }

    public DataSource pooledDataSource(DataSourceProperties.DataSourceConfig conf) {
        PooledDataSource dataSource = new PooledDataSource();
        dataSource.setPoolMaximumActiveConnections(50);
        dataSource.setPoolPingEnabled(true);
        dataSource.setPoolPingQuery("Select 1");
        dataSource.setPoolMaximumIdleConnections(10);
        dataSource.setPoolPingConnectionsNotUsedFor(10000);
        dataSource.setPoolTimeToWait(10000);

        dataSource.setDriver(conf.getDriverClassName());
        dataSource.setUrl(conf.getUrl());
        dataSource.setUsername(conf.getUsername());
        dataSource.setPassword(conf.getPassword());
        return dataSource;
    }


    @Primary
    @Bean(name = SQL_SESSION_FACTORY_NAME_1)
    public SqlSessionFactory sqlSessionFactory1() throws Exception {
        SqlSessionFactoryBean sqlSessionFactoryBean = new SqlSessionFactoryBean();
        //sqlSessionFactoryBean.setTypeHandlersPackage(DateTimeTypeHandler.class.getPackage().getName());
        sqlSessionFactoryBean.setDataSource(getDataSource());
        SqlSessionFactory sqlSessionFactory = sqlSessionFactoryBean.getObject();
        sqlSessionFactory.getConfiguration().setMapUnderscoreToCamelCase(true);
        sqlSessionFactory.getConfiguration().setJdbcTypeForNull(JdbcType.NULL);
        return sqlSessionFactory;
    }



    @Bean
    public MapperScannerConfigurer mapperScannerConfigurer1() {
        MapperScannerConfigurer configurer = new MapperScannerConfigurer();
        configurer.setBasePackage(MAPPERS_PACKAGE_NAME_1);
        configurer.setSqlSessionFactoryBeanName(SQL_SESSION_FACTORY_NAME_1);
        return configurer;
    }

}
