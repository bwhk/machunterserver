package tho.dev.tools.typeHandler;

import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * MyBatis Enum Type Handler (so lower and smaller case is unimportant.
 */
public class EnumTypeHandler<E extends Enum<E>> extends BaseTypeHandler<E> {

    /** Der Klassentyp der Enumeration. */
    private Class<E> type;

    /**
     * Konstruktor der die konkrete Enumeration Klasse setzt. Wenn der Typ NULL ist wird eine Exception geworfen.
     * @param newType Enumeration Typ.
     */
    public EnumTypeHandler(Class<E> newType) {
        if (newType == null) {
            throw new IllegalArgumentException("Type argument cannot be null");
        }
        this.type = newType;
    }

    @Override
    public void setNonNullParameter(PreparedStatement ps, int i, E parameter, JdbcType jdbcType) throws SQLException {
        if (jdbcType == null) {
            ps.setString(i, parameter.name().toLowerCase());
        } else {
            ps.setObject(i, parameter.name().toLowerCase(), jdbcType.TYPE_CODE);
        }
    }

    @Override
    public E getNullableResult(ResultSet rs, String columnName) throws SQLException {
        String s = rs.getString(columnName);
        try {
            return s == null ? null : Enum.valueOf(type, s.toUpperCase());
        } catch (IllegalArgumentException e) {
            return null;
        }
    }

    @Override
    public E getNullableResult(ResultSet rs, int columnIndex) throws SQLException {
        String s = rs.getString(columnIndex);
        try {
            return s == null ? null : Enum.valueOf(type, s.toUpperCase());
        } catch (IllegalArgumentException e) {
            return null;
        }
    }

    @Override
    public E getNullableResult(CallableStatement cs, int columnIndex) throws SQLException {
        String s = cs.getString(columnIndex);
        try {
            return s == null ? null : Enum.valueOf(type, s.toUpperCase());
        } catch (IllegalArgumentException e) {
            return null;
        }
    }
}
