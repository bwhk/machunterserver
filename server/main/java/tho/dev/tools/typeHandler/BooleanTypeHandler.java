package tho.dev.tools.typeHandler;

import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Boolean Type Handler for mySql char enums
 */
public class BooleanTypeHandler extends BaseTypeHandler<Boolean> {

    private static final String TRUE = "Y";
    private static final String FALSE = "N";

    @Override
    public void setNonNullParameter(PreparedStatement preparedStatement, int i, Boolean aBoolean, JdbcType jdbcType) throws SQLException {
        preparedStatement.setString(i, convertBooleanToString(aBoolean));
    }

    @Override
    public Boolean getNullableResult(ResultSet resultSet, String s) throws SQLException {
        return convertStringToBoolean(resultSet.getString(s));
    }

    @Override
    public Boolean getNullableResult(ResultSet resultSet, int i) throws SQLException {
        return convertStringToBoolean(resultSet.getString(i));
    }

    @Override
    public Boolean getNullableResult(CallableStatement callableStatement, int i) throws SQLException {
        return convertStringToBoolean(callableStatement.getString(i));
    }

    private String convertBooleanToString(Boolean value) {
        if (value != null) {
            if (value) {
                return TRUE;
            } else {
                return FALSE;
            }
        }
        return null;
    }

    private Boolean convertStringToBoolean(String s) {
        if (s == null) {
            return null;
        }
        return s.equals(TRUE);
    }

}
