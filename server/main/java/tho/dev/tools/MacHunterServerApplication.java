package tho.dev.tools;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import tho.dev.tools.fascade.DeviceController;

/**
 * Start Application class.
 */
@SpringBootApplication
@MapperScan("tho.dev.tools.mapper")
@EnableAutoConfiguration
public class MacHunterServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(MacHunterServerApplication.class, args);
	}
}
