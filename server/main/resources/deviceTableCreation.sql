CREATE TABLE `devices` (
  `id` smallint(6) unsigned NOT NULL AUTO_INCREMENT,
  `macIdentifier` varchar(48) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `active` enum('N','Y') NOT NULL DEFAULT 'N',
  `ip` text CHARACTER SET utf8 COLLATE utf8_bin,
  `description` text CHARACTER SET utf8 COLLATE utf8_bin,
  `snmpVersion` enum('V1','V2C', 'V3') NULL,
  `account_id` smallint(6) unsigned NOT NULL,
  `user_id` smallint(6) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;