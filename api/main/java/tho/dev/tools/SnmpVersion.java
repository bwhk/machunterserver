package tho.dev.tools;

/**
 * Created by tonyhorst on 04.12.16.
 */
public enum SnmpVersion {

    V1,
    V2C,
    V3
}
