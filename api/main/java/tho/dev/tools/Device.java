package tho.dev.tools;

import javax.validation.constraints.NotNull;

/**
 * Device Entity.
 */
public class Device {

    /** Database ID from device entry. */
    private Long id;
    /** Mac OUI Address. */
    @NotNull(message = "You must set a mac identifier oid address")
    private String macIdentifier;
    /** Name from this device. */
    private String name;
    /** IP Address. */
    private String ip;
    /** Description. */
    private String description;
    /** Configured snmp version on device. If null then no snmp control/trap receive is possible. */
    private SnmpVersion snmpVersion;
    /** Is this device control active? */
    private boolean active;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMacIdentifier() {
        return macIdentifier;
    }

    public void setMacIdentifier(String macIdentifier) {
        this.macIdentifier = macIdentifier;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public SnmpVersion getSnmpVersion() {
        return snmpVersion;
    }

    public void setSnmpVersion(SnmpVersion snmpVersion) {
        this.snmpVersion = snmpVersion;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Device device = (Device) o;

        if (active != device.active) return false;
        if (id != null ? !id.equals(device.id) : device.id != null) return false;
        if (macIdentifier != null ? !macIdentifier.equals(device.macIdentifier) : device.macIdentifier != null)
            return false;
        if (name != null ? !name.equals(device.name) : device.name != null) return false;
        if (ip != null ? !ip.equals(device.ip) : device.ip != null) return false;
        if (description != null ? !description.equals(device.description) : device.description != null) return false;
        return snmpVersion == device.snmpVersion;

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (macIdentifier != null ? macIdentifier.hashCode() : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (ip != null ? ip.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (snmpVersion != null ? snmpVersion.hashCode() : 0);
        result = 31 * result + (active ? 1 : 0);
        return result;
    }
}
